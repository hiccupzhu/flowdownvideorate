/**
 * SECTION:element-flowdownvideorate
 *
 * FIXME:Describe flowdownvideorate here.
 *
 * <refsect2>
 * <title>Example launch line</title>
 * |[
 * gst-launch -v -m fakesrc ! flowdownvideorate ! fakesink silent=TRUE
 * ]|
 * </refsect2>
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <gst/gst.h>

#include "gstflowdownvideorate.h"
#include "gdef.h"

GST_DEBUG_CATEGORY_STATIC (gst_flowdown_videorate_debug);
#define GST_CAT_DEFAULT gst_flowdown_videorate_debug

/* Filter signals and args */
enum
{
  /* FILL ME */
  LAST_SIGNAL
};

enum
{
  PROP_0,
  PROP_SILENT
};

/* the capabilities of the inputs and outputs.
 *
 * describe the real formats here.
 */
static GstStaticPadTemplate sink_factory = GST_STATIC_PAD_TEMPLATE ("sink",
    GST_PAD_SINK,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS ("video/x-raw-yuv; video/x-raw-rgb; image/jpeg; image/png")
    );

static GstStaticPadTemplate src_factory = GST_STATIC_PAD_TEMPLATE ("src",
    GST_PAD_SRC,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS ("video/x-raw-yuv; video/x-raw-rgb; image/jpeg; image/png")
    );

GST_BOILERPLATE (GstFlowdownVideorate, gst_flowdown_videorate, GstElement, GST_TYPE_ELEMENT);

//////////////////////////////////////////////////////////////////////////////////////////////////
static void     gst_flowdown_videorate_finalize (GstFlowdownVideorate * filter);
static void     gst_flowdown_videorate_dispose  (GObject        *object);
static gboolean gst_flowdown_videorate_set_caps (GstPad * pad, GstCaps * caps);
static gboolean gst_flowdown_videorate_transformcaps (GstPad * in_pad, GstCaps * in_caps, GstPad * out_pad, GstCaps ** out_caps);
static GstCaps* gst_flowdown_videorate_get_caps (GstPad * pad);
static GstFlowReturn gst_flowdown_videorate_chain (GstPad * pad, GstBuffer * buf);
static gboolean gst_flowdown_videorate_event    (GstPad * pad, GstEvent * event);
static gboolean gst_flowdown_videorate_query            (GstPad * pad, GstQuery * query);
static GstStateChangeReturn gst_flowdown_videorate_change_state (GstElement * element, GstStateChange transition);


static void gst_flowdown_videorate_set_property (GObject * object, guint prop_id,
    const GValue * value, GParamSpec * pspec);
static void gst_flowdown_videorate_get_property (GObject * object, guint prop_id,
    GValue * value, GParamSpec * pspec);

//////////////////////////////////////////////////////////////////////////////////////////////////
/* GObject vmethod implementations */

static void
gst_flowdown_videorate_base_init (gpointer gclass)
{
    GstPadTemplate *sink_templ, *src_templ;
    GstElementClass *element_class = GST_ELEMENT_CLASS (gclass);
    GstFlowdownVideorateClass *fclass = GST_FLOWDOWNVIDEORATE_CLASS(gclass);

    gst_element_class_set_details_simple(element_class,
            "FlowdownVideorate",
            "FIXME:Generic",
            "FIXME:Generic Template Element",
            "szhu <<user@hostname.org>>");
    gst_element_class_add_pad_template (element_class, gst_static_pad_template_get (&src_factory));
    gst_element_class_add_pad_template (element_class, gst_static_pad_template_get (&sink_factory));
}

/* initialize the flowdownvideorate's class */
static void
gst_flowdown_videorate_class_init (GstFlowdownVideorateClass * klass)
{
  GObjectClass *gobject_class;
  GstElementClass *gstelement_class;

  gobject_class = (GObjectClass *) klass;
  gstelement_class = (GstElementClass *) klass;

  gobject_class->finalize     = GST_DEBUG_FUNCPTR((GObjectFinalizeFunc)gst_flowdown_videorate_finalize);
  gobject_class->dispose      = GST_DEBUG_FUNCPTR(gst_flowdown_videorate_dispose);
  gobject_class->set_property = GST_DEBUG_FUNCPTR(gst_flowdown_videorate_set_property);
  gobject_class->get_property = GST_DEBUG_FUNCPTR(gst_flowdown_videorate_get_property);

  g_object_class_install_property (gobject_class, PROP_SILENT,
      g_param_spec_boolean ("silent", "Silent", "Produce verbose output ?",
          FALSE, G_PARAM_READWRITE));

  gstelement_class->change_state = GST_DEBUG_FUNCPTR(gst_flowdown_videorate_change_state);
}

static void
gst_flowdown_videorate_init (GstFlowdownVideorate * filter,
    GstFlowdownVideorateClass * gclass)
{
    filter->sinkpad = gst_pad_new_from_static_template (&sink_factory, "sink");
    gst_pad_set_event_function(filter->sinkpad, gst_flowdown_videorate_event);
//      gst_pad_set_getcaps_function (filter->sinkpad, gst_pad_proxy_getcaps);
    gst_pad_set_setcaps_function (filter->sinkpad, gst_flowdown_videorate_set_caps);
    gst_pad_set_getcaps_function (filter->sinkpad, gst_flowdown_videorate_get_caps);
    gst_pad_set_chain_function   (filter->sinkpad, gst_flowdown_videorate_chain);
    gst_element_add_pad (GST_ELEMENT (filter), filter->sinkpad);

    filter->srcpad = gst_pad_new_from_static_template (&src_factory, "src");
    gst_pad_set_getcaps_function (filter->srcpad, gst_flowdown_videorate_query);
//    gst_pad_set_getcaps_function (filter->srcpad,  gst_pad_proxy_getcaps);
    gst_pad_set_setcaps_function (filter->srcpad, gst_flowdown_videorate_set_caps);
    gst_pad_set_getcaps_function (filter->srcpad, gst_flowdown_videorate_get_caps);
    gst_element_add_pad (GST_ELEMENT (filter), filter->srcpad);


    filter->silent = FALSE;
    filter->base_time = 0;
    filter->caps = NULL;
    filter->to_rate_denominator = 0;
    filter->to_rate_numerator = 0;
    filter->from_rate_denominator = 0;
    filter->from_rate_numerator = 0;


    GST_DEBUG_FUNCPTR(gst_flowdown_videorate_set_caps);
    GST_DEBUG_FUNCPTR(gst_flowdown_videorate_get_caps);
    GST_DEBUG_FUNCPTR(gst_flowdown_videorate_chain);
    GST_DEBUG_FUNCPTR(gst_flowdown_videorate_event);

    gst_segment_init (&filter->segment, GST_FORMAT_TIME);
}

static void
gst_flowdown_videorate_finalize (GstFlowdownVideorate * filter)
{
    G_OBJECT_CLASS (parent_class)->finalize (G_OBJECT (filter));
}


static void
gst_flowdown_videorate_dispose  (GObject        *object)
{
    GstFlowdownVideorate *filter;

    filter = GST_FLOWDOWNVIDEORATE(object);


    G_OBJECT_CLASS (parent_class)->dispose (object);
}


/* GstElement vmethod implementations */

/* this function handles the link with other elements */
static gboolean
gst_flowdown_videorate_set_caps (GstPad * pad, GstCaps * caps)
{
    GstFlowdownVideorate *videorate;
    GstStructure *structure;
    gboolean ret = TRUE;
    GstPad *otherpad, *opeer;
    gint rate_numerator, rate_denominator;

    av_print("####### pad=%s SET caps=%s\n", gst_pad_get_name(pad), gst_caps_to_string(caps));

    videorate = GST_FLOWDOWNVIDEORATE(gst_pad_get_parent (pad));


    structure = gst_caps_get_structure (caps, 0);
    if (!gst_structure_get_fraction (structure, "framerate", &rate_numerator, &rate_denominator))
        goto no_framerate;

    if (pad == videorate->srcpad) {
        videorate->to_rate_numerator = rate_numerator;
        videorate->to_rate_denominator = rate_denominator;
        otherpad = videorate->sinkpad;
    } else {
        videorate->from_rate_numerator = rate_numerator;
        videorate->from_rate_denominator = rate_denominator;
        otherpad = videorate->srcpad;
    }

//    return gst_pad_set_caps(videorate->srcpad, caps);

    /* now try to find something for the peer */
    opeer = gst_pad_get_peer (otherpad);
    if (opeer) {
        ret = gst_pad_accept_caps (opeer, caps);
        av_print("    ### gst_pad_accept_caps() %s!\n", ret ? "OK" : "FAILED");
        if (ret) {
            /* the peer accepts the caps as they are */
            gst_pad_set_caps (otherpad, caps);

            ret = TRUE;
        } else {
            GstCaps *peercaps;
            GstCaps *intersect;
            GstCaps *transform = NULL;

            ret = FALSE;

            /* see how we can transform the input caps */
            if (!gst_flowdown_videorate_transformcaps (pad, caps, otherpad, &transform))
                goto no_transform;

            /* see what the peer can do */
            peercaps = gst_pad_get_caps (opeer);

            /* filter against our possibilities */
            intersect = gst_caps_intersect (peercaps, transform);
            gst_caps_unref (peercaps);
            gst_caps_unref (transform);

            GST_DEBUG ("intersect %" GST_PTR_FORMAT, intersect);

            /* take first possibility */
            caps = gst_caps_copy_nth (intersect, 0);
            gst_caps_unref (intersect);
            structure = gst_caps_get_structure (caps, 0);

            /* and fixate */
            gst_structure_fixate_field_nearest_fraction (structure, "framerate", rate_numerator, rate_denominator);
            gst_structure_get_fraction (structure, "framerate", &rate_numerator, &rate_denominator);

            if (otherpad == videorate->srcpad) {
                videorate->to_rate_numerator = rate_numerator;
                videorate->to_rate_denominator = rate_denominator;
            } else {
                videorate->from_rate_numerator = rate_numerator;
                videorate->from_rate_denominator = rate_denominator;
            }

            av_print("+++    ### %s Set caps=%s\n",
                    gst_pad_get_name(otherpad), gst_caps_to_string(caps));
            ret = gst_pad_set_caps (otherpad, caps);
            av_print("---    ### %s Set caps %s\n                                      caps=%s\n",
                    gst_pad_get_name(otherpad), ret ? "OK" : "FAILED", gst_caps_to_string(gst_pad_get_caps(otherpad)));
            gst_caps_unref (caps);
            ret = TRUE;
        }
        gst_object_unref (opeer);
    }

done:
    gst_object_unref (videorate);
    return ret;

no_framerate:
    {
        GST_DEBUG_OBJECT (videorate, "no framerate specified");
        goto done;
    }
no_transform:
    {
        GST_DEBUG_OBJECT (videorate, "no framerate transform possible");
        ret = FALSE;
        goto done;
    }
}

static gboolean
gst_flowdown_videorate_transformcaps (GstPad * in_pad, GstCaps * in_caps,
    GstPad * out_pad, GstCaps ** out_caps)
{
  GstCaps *intersect;
  const GstCaps *in_templ;
  gint i;
  GSList *extra_structures = NULL;
  GSList *iter;

  in_templ = gst_pad_get_pad_template_caps (in_pad);
  intersect = gst_caps_intersect (in_caps, in_templ);

  /* all possible framerates are allowed */
  for (i = 0; i < gst_caps_get_size (intersect); i++) {
    GstStructure *structure;

    structure = gst_caps_get_structure (intersect, i);

    if (gst_structure_has_field (structure, "framerate")) {
      GstStructure *copy_structure;

      copy_structure = gst_structure_copy (structure);
      gst_structure_set (copy_structure,
          "framerate", GST_TYPE_FRACTION_RANGE, 0, 1, G_MAXINT, 1, NULL);
      extra_structures = g_slist_append (extra_structures, copy_structure);
    }
  }

  /* append the extra structures */
  for (iter = extra_structures; iter != NULL; iter = g_slist_next (iter)) {
    gst_caps_append_structure (intersect, (GstStructure *) iter->data);
  }
  g_slist_free (extra_structures);

  *out_caps = intersect;

  return TRUE;
}


static GstCaps*
gst_flowdown_videorate_get_caps (GstPad * pad)
{
    GstFlowdownVideorate *videorate;
    GstPad *otherpad;
    GstCaps *caps;

    videorate = GST_FLOWDOWNVIDEORATE (GST_PAD_PARENT (pad));

    otherpad = (pad == videorate->srcpad) ? videorate->sinkpad : videorate->srcpad;

    /* we can do what the peer can */
    caps = gst_pad_peer_get_caps (otherpad);
    if (caps) {
        GstCaps *transform;

        gst_flowdown_videorate_transformcaps (otherpad, caps, pad, &transform);
        gst_caps_unref (caps);
        caps = transform;
    } else {
        /* no peer, our padtemplate is enough then */
        caps = gst_caps_copy (gst_pad_get_pad_template_caps (pad));
    }

    av_print("##***## pad=%s GET caps=%s\n", gst_pad_get_name(pad), gst_caps_to_string(caps));

    return caps;
}

/* chain function
 * this function does the actual processing
 */
static GstFlowReturn
gst_flowdown_videorate_chain (GstPad * pad, GstBuffer * buf)
{
    GstFlowReturn ret;
    GstFlowdownVideorate *filter;
    GstBuffer *outbuf;

    filter = GST_FLOWDOWNVIDEORATE (GST_OBJECT_PARENT (pad));

    if (filter->silent == TRUE)
        return gst_pad_push (filter->srcpad, buf);

    outbuf = gst_buffer_make_metadata_writable(buf);

    gst_buffer_set_caps(outbuf, filter->srcpad->caps);


    ret = gst_pad_push (filter->srcpad, outbuf);

    return ret;
}

static gboolean
gst_flowdown_videorate_event (GstPad * pad, GstEvent * event)
{
    GstFlowdownVideorate *videorate;
    gboolean ret;

    videorate = GST_FLOWDOWNVIDEORATE (gst_pad_get_parent (pad));

    switch (GST_EVENT_TYPE (event)) {
    case GST_EVENT_NEWSEGMENT:
    {
        gint64 start, stop, time;
        gdouble rate, arate;
        gboolean update;
        GstFormat format;

        gst_event_parse_new_segment_full (event, &update, &rate, &arate, &format,
                &start, &stop, &time);

        if (format != GST_FORMAT_TIME)
            goto format_error;

        /* We just want to update the accumulated stream_time  */
        gst_segment_set_newsegment_full (&videorate->segment, update, rate, arate,
                format, start, stop, time);

        break;
    }
    case GST_EVENT_EOS:
        /* flush last queued frame */
        GST_DEBUG_OBJECT (videorate, "Got EOS");
        break;
    case GST_EVENT_FLUSH_STOP:
        /* also resets the segment */
        GST_DEBUG_OBJECT (videorate, "Got FLUSH_STOP");
        break;
    default:
        break;
    }

    ret = gst_pad_push_event (videorate->srcpad, event);

done:
    gst_object_unref (videorate);

    return ret;

    /* ERRORS */
format_error:
    {
        GST_WARNING_OBJECT (videorate,
                "Got segment but doesn't have GST_FORMAT_TIME value");
        gst_event_unref (event);
        ret = FALSE;
        goto done;
    }
}

static gboolean
gst_flowdown_videorate_query (GstPad * pad, GstQuery * query)
{
    GstFlowdownVideorate *videorate;
    gboolean res = FALSE;

    videorate = GST_FLOWDOWNVIDEORATE (gst_pad_get_parent (pad));

    switch (GST_QUERY_TYPE (query)) {
    case GST_QUERY_LATENCY:
    {
        GstClockTime min, max;
        gboolean live;
        guint64 latency;
        GstPad *peer;

        if ((peer = gst_pad_get_peer (videorate->sinkpad))) {
            if ((res = gst_pad_query (peer, query))) {
                gst_query_parse_latency (query, &live, &min, &max);


                if (videorate->from_rate_numerator != 0) {
                    /* add latency. We don't really know since we hold on to the frames
                     * until we get a next frame, which can be anything. We assume
                     * however that this will take from_rate time. */
                    latency = gst_util_uint64_scale (GST_SECOND,
                            videorate->from_rate_denominator,
                            videorate->from_rate_numerator);
                } else {
                    /* no input framerate, we don't know */
                    latency = 0;
                }

                GST_DEBUG_OBJECT (videorate, "Our latency: %"
                        GST_TIME_FORMAT, GST_TIME_ARGS (latency));

                min += latency;
                if (max != -1)
                    max += latency;

                gst_query_set_latency (query, live, min, max);
            }
            gst_object_unref (peer);
        }
        break;
    }
    default:
        res = gst_pad_query_default (pad, query);
        break;
    }
    gst_object_unref (videorate);

    return res;
}

static GstStateChangeReturn
gst_flowdown_videorate_change_state (GstElement * element, GstStateChange transition)
{
  GstStateChangeReturn ret;
  GstFlowdownVideorate *filter;

  filter = GST_FLOWDOWNVIDEORATE(element);

  GST_WARNING_OBJECT(filter, "state=%s-->%s\n", gst_element_state_change_return_get_name(transition >> 3), gst_element_state_change_return_get_name(transition & 7));

  switch (transition) {
    case GST_STATE_CHANGE_NULL_TO_READY:
      break;
    case GST_STATE_CHANGE_READY_TO_PAUSED:
      break;
    case GST_STATE_CHANGE_PAUSED_TO_PLAYING:
    case GST_STATE_CHANGE_PLAYING_TO_PAUSED:
      break;
    case GST_STATE_CHANGE_PAUSED_TO_READY:
      break;
    default:
      break;
  }

  ret = GST_ELEMENT_CLASS (parent_class)->change_state (element, transition);
  if (ret == GST_STATE_CHANGE_FAILURE)
    return ret;

  switch (transition) {
    case GST_STATE_CHANGE_PLAYING_TO_PAUSED:
      break;
    case GST_STATE_CHANGE_PAUSED_TO_READY:
    case GST_STATE_CHANGE_READY_TO_NULL:
      break;
    default:
      break;
  }

  GST_WARNING_OBJECT(filter, "state=%d-->%d ret=%d\n", transition >> 3, transition & 7, ret);

  return ret;
}

static void
gst_flowdown_videorate_set_property (GObject * object, guint prop_id,
    const GValue * value, GParamSpec * pspec)
{
  GstFlowdownVideorate *filter = GST_FLOWDOWNVIDEORATE (object);

  switch (prop_id) {
    case PROP_SILENT:
      filter->silent = g_value_get_boolean (value);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }
}

static void
gst_flowdown_videorate_get_property (GObject * object, guint prop_id,
    GValue * value, GParamSpec * pspec)
{
  GstFlowdownVideorate *filter = GST_FLOWDOWNVIDEORATE (object);

  switch (prop_id) {
    case PROP_SILENT:
      g_value_set_boolean (value, filter->silent);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }
}



















/* entry point to initialize the plug-in
 * initialize the plug-in itself
 * register the element factories and other features
 */
static gboolean
flowdownvideorate_init (GstPlugin * flowdownvideorate)
{
  /* debug category for fltering log messages
   *
   * exchange the string 'Template flowdownvideorate' with your description
   */
  GST_DEBUG_CATEGORY_INIT (gst_flowdown_videorate_debug, "flowdownvideorate",
      0, "Template flowdownvideorate");

  return gst_element_register (flowdownvideorate, "flowdownvideorate", GST_RANK_NONE,
      GST_TYPE_FLOWDOWNVIDEORATE);
}

/* PACKAGE: this is usually set by autotools depending on some _INIT macro
 * in configure.ac and then written into and defined in config.h, but we can
 * just set it ourselves here in case someone doesn't use autotools to
 * compile this code. GST_PLUGIN_DEFINE needs PACKAGE to be defined.
 */
#ifndef PACKAGE
#define PACKAGE "myfirstflowdownvideorate"
#endif

/* gstreamer looks for this structure to register flowdownvideorates
 *
 * exchange the string 'Template flowdownvideorate' with your flowdownvideorate description
 */
GST_PLUGIN_DEFINE (
    GST_VERSION_MAJOR,
    GST_VERSION_MINOR,
    "flowdownvideorate",
    "Template flowdownvideorate",
    flowdownvideorate_init,
    VERSION,
    "LGPL",
    "GStreamer",
    "http://gstreamer.net/"
)
