CFLAGS=\
	-DPACKAGE='"mpegdemux"' -DGST_PACKAGE_NAME='"GStreamer"' \
	-DGST_PACKAGE_ORIGIN='"http://gstreamer.net"' \
	-DVERSION='"1.0"' -DHAVE_USER_MTU -Wall -Wimplicit -g \
	$(shell pkg-config --cflags gstreamer-tag-0.10 gstreamer-base-0.10 liboil-0.3)
	
LDFLAGS=$(shell pkg-config --libs gstreamer-tag-0.10 gstreamer-base-0.10 liboil-0.3)
INSTALL_DIR=/usr/lib64/gstreamer-0.10/
PWD=$(shell pwd)

all:libflowdownvideorate.la

libflowdownvideorate.la:gstflowdownvideorate.lo
	libtool --mode=link gcc -module -shared -export-symbols-regex gst_plugin_desc $(LDFLAGS) -o $@ $+ -avoid-version -rpath $(INSTALL_DIR)

%.lo: %.c %.h
	libtool --mode=compile gcc $(CFLAGS) -fPIC -c -o $@ $<

%.lo: %.c
	libtool --mode=compile gcc $(CFLAGS) -fPIC -c -o $@ $<
	
.PHONY: install

install: .libs/libflowdownvideorate.so
	libtool --mode=install install $< $(INSTALL_DIR)
	
link:.libs/libflowdownvideorate.so
	ln -sf $(PWD)/$< $(INSTALL_DIR)

clean:
	rm -rf *.o *.lo *.a *.la .libs
