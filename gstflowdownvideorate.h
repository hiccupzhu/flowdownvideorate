#ifndef __GST_FLOWDOWNVIDEORATE_H__
#define __GST_FLOWDOWNVIDEORATE_H__

#include <gst/gst.h>

G_BEGIN_DECLS

/* #defines don't like whitespacey bits */
#define GST_TYPE_FLOWDOWNVIDEORATE \
  (gst_flowdown_videorate_get_type())
#define GST_FLOWDOWNVIDEORATE(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST((obj),GST_TYPE_FLOWDOWNVIDEORATE,GstFlowdownVideorate))
#define GST_FLOWDOWNVIDEORATE_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST((klass),GST_TYPE_FLOWDOWNVIDEORATE,GstFlowdownVideorateClass))
#define GST_IS_FLOWDOWNVIDEORATE(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE((obj),GST_TYPE_FLOWDOWNVIDEORATE))
#define GST_IS_FLOWDOWNVIDEORATE_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE((klass),GST_TYPE_FLOWDOWNVIDEORATE))

typedef struct _GstFlowdownVideorate      GstFlowdownVideorate;
typedef struct _GstFlowdownVideorateClass GstFlowdownVideorateClass;

struct _GstFlowdownVideorate
{
  GstElement element;

  GstPad *sinkpad, *srcpad;

  gboolean silent;

  GstCaps *caps;
  guint64 base_time;
  /* video state */
  gint from_rate_numerator, from_rate_denominator;
  gint to_rate_numerator, to_rate_denominator;


  GstSegment segment;
};

struct _GstFlowdownVideorateClass 
{
  GstElementClass parent_class;
};

GType gst_flowdown_videorate_get_type (void);

G_END_DECLS

#endif /* __GST_FLOWDOWNVIDEORATE_H__ */
